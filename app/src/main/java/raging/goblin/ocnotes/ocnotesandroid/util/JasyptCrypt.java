package raging.goblin.ocnotes.ocnotesandroid.util;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;

public class JasyptCrypt {

	private static final String ENCRYPTION_PASSWORD = "RagingGoblinRagesAtTheWorld";
	private static final String PADDING = "RAGING_99999_GOBLIN_";

	public static String decryptPassword(String encryptedPassword) throws EncryptionOperationNotPossibleException {
		encryptedPassword = encryptedPassword.replace(PADDING, "");
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(ENCRYPTION_PASSWORD);
		return textEncryptor.decrypt(encryptedPassword);
	}

	public static String encryptPassword(char[] password) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(ENCRYPTION_PASSWORD);
		return PADDING + textEncryptor.encrypt(new String(password));
	}

	public static boolean isEncrypted(String password) {
		return password.startsWith(PADDING);
	}

}
