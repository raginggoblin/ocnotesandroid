package raging.goblin.ocnotes.ocnotesandroid.data;

import android.content.Context;
import android.database.Cursor;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.ocnotes.sync.synchronization.OcNotesStorage;
import raging.goblin.ocnotes.sync.webserviceclient.HttpErrorCodeException;

public class DbStorage implements OcNotesStorage {

	private DbAdapter db;
	private List<OcNote> ocNotes;

	public DbStorage(Context context) {
		db = new DbAdapter(context);
	}

	public void start() {
		db.open();
		ocNotes = fetchOcNotesFromDb();
	}

	public void stop() {
		writeOcNotesToDb();
		db.close();
	}

    @Override
    public List<OcNote> updateOcNotes(List<OcNote> ocNotes, String username, String password) throws JsonGenerationException, JsonMappingException, UnsupportedEncodingException, URISyntaxException, IOException, HttpErrorCodeException {
        this.ocNotes.removeAll(ocNotes);
        this.ocNotes.addAll(ocNotes);
        return ocNotes;
    }

    @Override
    public List<OcNote> addOcNotes(List<OcNote> ocNotes, String username, String password) throws JsonParseException, JsonMappingException, IOException, URISyntaxException, HttpErrorCodeException {
        this.ocNotes.addAll(ocNotes);
        return ocNotes;
    }

    @Override
    public void removeOcNotes(List<OcNote> ocNotes, String username, String password) throws JsonGenerationException, JsonMappingException, UnsupportedEncodingException, URISyntaxException, IOException, HttpErrorCodeException {
        this.ocNotes.removeAll(ocNotes);
    }

    @Override
    public List<OcNote> getOcNotes(String username, String password) throws JsonParseException, JsonMappingException, IOException, HttpErrorCodeException, URISyntaxException {
        return ocNotes;
    }

    @Override
    public void updateId(int oldId, int newId, String username, String password) throws JsonParseException, JsonMappingException, IOException, HttpErrorCodeException, URISyntaxException {
        for(OcNote ocNote: ocNotes) {
            if (ocNote.getId() == oldId) {
                ocNotes.remove(ocNote);
                OcNote newNote = new OcNote(newId, ocNote.getModified(), ocNote.getTitle(), ocNote.getContent());
                ocNotes.add(newNote);
            }
        }
    }

	private List<OcNote> fetchOcNotesFromDb() {
		List<OcNote> ocNotes = new ArrayList<OcNote>();
		Cursor cursor = db.fetchAllNotes();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			int ownCloudIdColumn = cursor.getColumnIndex(DbAdapter.KEY_OWNCLOUD_ID);
			int ownCloudId = cursor.getInt(ownCloudIdColumn);
			int subjectColumn = cursor.getColumnIndex(DbAdapter.KEY_TITLE);
			String subject = cursor.getString(subjectColumn);
			int descriptionColumn = cursor.getColumnIndex(DbAdapter.KEY_BODY);
			String description = cursor.getString(descriptionColumn);
			int dateModifiedColumn = cursor.getColumnIndex(DbAdapter.KEY_TIMESTAMP);
			long dateModified = cursor.getLong(dateModifiedColumn);
			ocNotes.add(new OcNote(ownCloudId, dateModified, subject, description));
			cursor.moveToNext();
		}
		return ocNotes;
	}

	private void writeOcNotesToDb() {
		db.deleteAllNotes();
        for (OcNote ocNote : ocNotes) {
            db.createNote(ocNote.getTitle(), ocNote.getContent(), ocNote.getModified(),
                    ocNote.getId());
        }
	}

}
