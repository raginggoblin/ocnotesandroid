package raging.goblin.ocnotes.ocnotesandroid.util;

import android.util.Log;

import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;

public class AndroidLogger implements Logger {

	private String logTag;

	public AndroidLogger(String logTag) {
		this.logTag = logTag;
	}

	@Override
	public void log(LogLevel level, String message, Exception e) {
		switch (level) {
			case DEBUG:
				Log.d(logTag, message, e);
				break;
			case INFO:
				Log.i(logTag, message, e);
				break;
			case WARN:
				Log.w(logTag, message, e);
				break;
			case ERROR:
			default:
				Log.e(logTag, message, e);
				break;
		}
	}

	@Override
	public void log(LogLevel level, String message) {
		switch (level) {
			case DEBUG:
				Log.d(logTag, message);
				break;
			case INFO:
				Log.i(logTag, message);
				break;
			case WARN:
				Log.w(logTag, message);
				break;
			case ERROR:
			default:
				Log.e(logTag, message);
				break;
		}
	}

}
