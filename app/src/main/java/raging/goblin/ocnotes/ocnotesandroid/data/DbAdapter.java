package raging.goblin.ocnotes.ocnotesandroid.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations for the notepad example, and gives the
 * ability to list all notes as well as retrieve or modify a specific note.
 * <p/>
 * This has been improved from the first version of this tutorial through the addition of better error handling and also
 * using returning a Cursor instead of using a collection of inner classes (which is less scalable and not recommended).
 */
public class DbAdapter {

	public static final String KEY_TITLE = "title";
	public static final String KEY_BODY = "body";
	public static final String KEY_TIMESTAMP = "timestamp";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_OWNCLOUD_ID = "owncloudid";

	private static final String TAG = "DbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	/**
	 * Database creation sql statement
	 */
	private static final String DATABASE_NAME = "ocnotesandroid";
	private static final String DATABASE_TABLE = "ocnotes";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = "create table " + DATABASE_NAME + " (" + KEY_ROWID + " integer primary key autoincrement, "
            + KEY_TITLE + " text not null, " + KEY_BODY + " text, " + KEY_TIMESTAMP + " integer not null, " + KEY_OWNCLOUD_ID + " integer not null);";


    private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
				 + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}
	}

	/**
	 * Constructor - takes the context to allow the database to be opened/created
	 *
	 * @param ctx the Context within which to work
	 */
	public DbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	/**
	 * Open the notes database. If it cannot be opened, try to create a new instance of the database. If it cannot be
	 * created, throw an exception to signal the failure
	 *
	 * @return this (self reference, allowing this to be chained in an initialization call)
	 * @throws android.database.SQLException if the database could be neither opened or created
	 */
	public DbAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	/**
	 * Create a new note using the title and body provided. If the note is successfully created return the new rowId for
	 * that note, otherwise return a -1 to indicate failure.
	 *
	 * @param title the title of the note
	 * @param body  the body of the note
	 * @return rowId or -1 if failed
	 */
	public long createNote(String title, String body, long modificationDate, long ownCloudId) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TITLE, title);
		initialValues.put(KEY_BODY, body);
		initialValues.put(KEY_TIMESTAMP, modificationDate);
		initialValues.put(KEY_OWNCLOUD_ID, ownCloudId);

		return mDb.insert(DATABASE_TABLE, null, initialValues);
	}

	/**
	 * Delete the note with the given rowId
	 *
	 * @param rowId id of note to delete
	 * @return true if deleted, false otherwise
	 */
	public boolean deleteNote(long rowId) {
		return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	/**
	 * Deletes all notes from the database
	 */
	public void deleteAllNotes() {
		mDb.delete(DATABASE_TABLE, "1", null);
	}

	/**
	 * Return a Cursor over the list of all notes in the database
	 *
	 * @return Cursor over all notes
	 */
	public Cursor fetchAllNotes() {
		return mDb.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_TITLE, KEY_BODY, KEY_TIMESTAMP,
			 KEY_OWNCLOUD_ID}, null, null, null, null, null);
	}

	/**
	 * Return a Cursor positioned at the note that matches the given rowId
	 *
	 * @param rowId id of note to retrieve
	 * @return Cursor positioned to matching note, if found
	 * @throws android.database.SQLException if note could not be found/retrieved
	 */
	public Cursor fetchNote(long rowId) throws SQLException {

		Cursor mCursor = mDb.query(true, DATABASE_TABLE, new String[]{KEY_ROWID, KEY_TITLE, KEY_BODY,
			 KEY_TIMESTAMP, KEY_OWNCLOUD_ID}, KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/**
	 * Returns the rowid of a note that matches the infologId.
	 *
	 * @return the rowid of the note with infologId.
	 */
	public long fetchRowId(long infologId) {
		Cursor cursor = mDb.query(true, DATABASE_TABLE, new String[]{KEY_ROWID},
			 KEY_OWNCLOUD_ID + "=" + infologId, null, null, null, null, null);
		return cursor.getLong(cursor.getColumnIndex(KEY_ROWID));
	}

	/**
	 * Returns the modification date of the last modified note.
	 */
	public long fetchLastModificationDate() {
		Cursor cursor = mDb.query(true, DATABASE_TABLE, new String[]{KEY_TIMESTAMP}, null, null, null, null,
			 KEY_TIMESTAMP + " DESC", null);
		cursor.moveToFirst();
		return cursor.getLong(cursor.getColumnIndex(KEY_TIMESTAMP));
	}

	/**
	 * Update the note using the details provided. The note to be updated is specified using the rowId, and it is
	 * altered to use the title and body values passed in
	 *
	 * @param rowId            id of note to update
	 * @param title            value to set note title to
	 * @param body             value to set note body to
	 * @param modificationDate timestamp of last modification
	 * @return true if the note was successfully updated, false otherwise
	 */
	public boolean updateNote(long rowId, String title, String body, long modificationDate,
	                          long ownCloudId) {
		ContentValues args = new ContentValues();
		args.put(KEY_TITLE, title);
		args.put(KEY_BODY, body);
		args.put(KEY_TIMESTAMP, modificationDate);
		args.put(KEY_OWNCLOUD_ID, ownCloudId);

		return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}
}
